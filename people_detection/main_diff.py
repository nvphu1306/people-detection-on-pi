# import the necessary packages
from imutils.video import VideoStream
import argparse
import datetime
import imutils
import time
import cv2
import numpy as np


feature_params = dict( maxCorners = 100,
                       qualityLevel = 0.3,
                       minDistance = 7,
                       blockSize = 7 )

# Parameters for lucas kanade optical flow
lk_params = dict( winSize  = (15,15),
                  maxLevel = 2,
                  criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))
# Create some random colors
color = np.random.randint(0,255,(100,3))

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-v", "--video", help="path to the video file")
ap.add_argument("-a", "--min-area", type=int, default=50, help="minimum area size")
args = vars(ap.parse_args())

# if the video argument is None, then we are reading from webcam
if args.get("video", None) is None:
	vs = VideoStream(src=0).start()
	time.sleep(2.0)

else:
	vs = cv2.VideoCapture(args["video"])
firstFrame = None
# loop over the frames of the video

fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('output.avi',fourcc, 20.0, (640,480))

number_frame = 0

ret, old_frame = vs.read()
old_gray = cv2.cvtColor(old_frame, cv2.COLOR_BGR2GRAY)
mask = np.zeros_like(old_frame)

while True:
	frame = vs.read()
	frame = frame if args.get("video", None) is None else frame[1]
	text = "Unoccupied"
	if frame is None:
		break


	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	frame_gray = gray.copy()

	gray = cv2.GaussianBlur(gray, (21, 21), 0)
	kernel = np.ones((13,13),np.uint8)
	if firstFrame is None:
		firstFrame = gray
		continue


	frameDelta = cv2.absdiff(firstFrame, gray)
	thresh = cv2.threshold(frameDelta, 50, 255, cv2.THRESH_BINARY)[1]
	thresh = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)
	thresh = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)
	thresh = cv2.dilate(thresh, np.ones((3,3), np.uint8), iterations=2)


	cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)
	cnts = cnts[0] if imutils.is_cv2() else cnts[1]
	for c in cnts:
		if cv2.contourArea(c) < args["min_area"]:
			continue
		(x, y, w, h) = cv2.boundingRect(c)

		cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

	old_gray = frame_gray.copy()
	cv2.imshow("Security Feed", frame)
	#out.write(frame)
	#cv2.imshow("Thresh", thresh)
	cv2.imshow("Frame Delta", frameDelta)
	key = cv2.waitKey(100) & 0xFF
	if key == ord("q"):
		break
cv2.destroyAllWindows()





